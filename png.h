#ifdef PBL_PLATFORM_APLITE
#pragma once

#include <stdint.h>
#include <stdbool.h>
#include <pebble.h>

/**
 * Creates a gbitmap from a Resource ID which is specified in the appinfo.json.
 * This function will free the internal source memory to save on memory usage.
 * @param resource_id the appinfo resource id.
 * @return if a failure, the memory location returned will be empty and if success it will have a fully loaded gbitmap.
 */
GBitmap* gbitmap_create_with_png_resource(uint32_t resource_id);
/**
 * Creates a bitmap from memory.
 * This function will free the internal source memory to save on memory usage.
 * It will not free the passed in array to the data.
 * @param data an array of bytes that contains the png data
 * @param data_bytes the size of the data memory space ie array size.
 * @return if a failure, the memory location returned will be empty and if success it will have a fully loaded gbitmap.
 */
GBitmap* gbitmap_create_with_png_data(uint8_t *data, int data_bytes);

#endif // ifdef PBL_PLATFORM_APLITE
