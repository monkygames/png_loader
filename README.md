# png_loader

This library loads PNG images on the pebble watch.  It decompresses the PNG 
into a bitmap which can then be displayed on the watch.

This library is a 'fork' of the [pebble-faces](https://github.com/pebble-examples/pebble-faces) example code.  This library strips out the application side of the code to make it easy to incorperate the png loading functionaity in other Pebble apps.

## Installation
This library assumes you already have a pebble project created.

### Steps
* Create a directory for the library
```
mkdir <project_home>/libs
```
* Change directory
```
cd <project_home>/libs
```
* Download the source
```
git clone git@bitbucket.org:monkygames/png_loader.git
```
* Edit the wscript
```
ctx.pbl_program(source=ctx.path.ant_glob('src/**/*.c'), target=app_elf )
```
to
```
ctx.pbl_program(source=ctx.path.ant_glob('src/**/*.c') + ctx.path.ant_glob('libs/**/*.c'), target=app_elf )
```
* Header Files

Add the following your project header file
```
#include "../libs/png_loader/png.h"
```

## Functions

### gbitmap_create_with_png_resource
`GBitmap* gbitmap_create_with_png_resource(uint32_t resource_id)`

Creates a gbitmap from a Resource ID which is specified in the appinfo.json.
#### Params
 * `resource_id` the appinfo resource id.
#### Returns
 * if a failure, the memory location returned will be empty
 * if success it will have a fully loaded gbitmap.

### gbitmap_create_with_png_data
`GBitmap* gbitmap_create_with_png_data(uint8_t *data, int data_bytes)`

Creates a bitmap from memory.
This function will free the internal source memory to save on memory usage.
It will not free the passed in array to the data.

#### Params
 * `data` an array of bytes that contains the png data
 * `data_bytes` the size of the data memory space ie array size.

#### Returns
 * if a failure, the memory location returned will be empty
 * if success it will have a fully loaded gbitmap.


## Additional Information   
A downside of this version is the memory requirement: the uPNG library takes
some code space in your app and you will need enough memory to have the PNG and
the Bitmap loaded during decompression.

## Preparing images

To reduce size as much as possible you should prepare your PNGs to match the
size of the screen (or smaller) and to use only two colors. You should also
remove any extra information from the PNG.

Using [ImageMagick](http://www.imagemagick.org/) for example:

    convert myimage.png \
      -adaptive-resize '144x168>' \
      -fill '#FFFFFF00' -opaque none \
      -type Grayscale -colorspace Gray \
      -colors 2 -depth 1 \
      -define png:compression-level=9 -define png:compression-strategy=0 \
      -define png:exclude-chunk=all \
      myimage.pbl.png

Notes:

 - `-fill #FFFFFF00 -opaque none` makes the transparency white
 - `-adaptive-resize` with > at end means resize only if larger, and maintains aspect ration
 - we exclude png chunks to reduce size (like when image was made, author)

If you want to use [dithering](http://en.wikipedia.org/wiki/Dither) to simulate
Grey, you can use this command:

    convert myimage.png \
      -adaptive-resize '144x168>' \
      -fill '#FFFFFF00' -opaque none \
      -type Grayscale -colorspace Gray \
      -black-threshold 30% -white-threshold 70% \
      -ordered-dither 2x1 \
      -colors 2 -depth 1 \
      -define png:compression-level=9 -define png:compression-strategy=0 \
      -define png:exclude-chunk=all \
      myimage.pbl.png

### SDK 3.x

To convert your image to the palletized format (64 colors) used by Pebble Time, use:

    convert myimage.png \
      -adaptive-resize '144x168>' \
      -fill '#FFFFFF00' -opaque none \
      -dither FloydSteinberg \
      -remap pebble_colors_64.gif \
      -define png:compression-level=9 -define png:compression-strategy=0 \
      -define png:exclude-chunk=all \
      myimage.pbl.png

**Important:** You need ImageMagick to do the conversion above. Mac OS X ships
with GraphicsMagick which does not support the PNG options to compress and
remove un-needed informations.

## Resources

For more information about PNG on Pebbles, how to optimize memory usage and tips
on image processing, please refer to the [Advanced techniques
videos](https://developer.getpebble.com/events/developer-retreat-2014/) from the
Pebble Developer Retreat 2014.
